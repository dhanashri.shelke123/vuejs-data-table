import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../components/home'
//import Blog from '../components/blog'
//import Services from '../components/services'
// import Contact from '../components/contact'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/data-table',
    name: 'table',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../components/data-table/dataTable')
  },
  {
    path: '/home',
    name: 'home',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../components/home')
  },
  {
    path: '/warehousesTable',
    name: 'warehousesTable',
    component: () => import(/* webpackChunkName: "about" */ '../components/warehousesTable')
  },
  {
    path: '/warehousesDetails',
    name: 'warehousesDetails',
    component: () => import(/* webpackChunkName: "about" */ '../components/warehousesDetails')

  },
  {
    path: '/warehouesUpdate',
    name: 'warehouesUpdate',
    component: () => import(/* webpackChunkName: "about" */ '../components/warehouesUpdate')

  }

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
