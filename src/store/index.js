import Vue from 'vue';
import Vuex from 'vuex';


Vue.use(Vuex);


/*export const store = new Vuex.Store(
    state,
    mutations,
)*/
export const store = new Vuex.Store({
        state: {
            item: null,
            index: null,
        },
    mutations:  {

        setItem(state, data){
            console.log(state, data);
            state.item = data;
        },
        setIndex(state, data){
            console.log(state, data);
            state.index = data;
        }
    }
    }
)

